﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class formMantUser : Form
    {
        public formMantUser()
        {
            InitializeComponent();
        }
        //funcion para inicializar el codigo del nuevo usuario
        public string NewCode()
        {
            Random rnd = new Random();
            return rnd.Next(10000, 10099).ToString();
        }
        //funcion para agregar un nuevo usuario a la tabla
        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                string codigo = lblCodigo.Text;
                string dni = txtDni.Text;
                string nombre = txtNombre.Text;
                string apellido = txtApellido.Text;
                string direccion = txtDireccion.Text;
                string telefono = txtTelefono.Text;
                string email = txtEmail.Text;
                string fecha = dtpFecha.Text.Length == 0 ? 
                    DateTime.Now.ToString("dd/MM/yyyy") : dtpFecha.Value.ToString("dd/MM/yyyy");
                string departamento = cboCargo.SelectedItem.ToString();
                string cargo = cboCargo.SelectedItem.ToString();
                dgvUsuarios.Rows.Add(codigo, dni, nombre, apellido, 
                    direccion, telefono, email, fecha, departamento, cargo);
                lblCodigo.Text = $"U{NewCode()}";
                txtDni.Text = "";
                txtNombre.Text = "";
                txtApellido.Text = "";
                txtDireccion.Text = "";
                txtTelefono.Text = "";
                txtEmail.Text = "";
            }
            catch (Exception err)
            {
                MessageBox.Show("Ocurrió un error: {0}", err.ToString());
            }
            
        }
        //on load window form?????
        private void Form1_Load(object sender, EventArgs e)
        {
            lblCodigo.Text = $"U{NewCode()}";
        }
        //funcion para borrar las filas seleccionadas por el usuario en la tabla
        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (dgvUsuarios.SelectedRows.Count > 0)
            {
                DialogResult dialogResult = MessageBox.Show("¿Estás seguro que deseas eliminar las filas seleccionadas?",
                    "Eliminar filas", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    foreach (DataGridViewRow item in dgvUsuarios.SelectedRows)
                    {
                        dgvUsuarios.Rows.RemoveAt(item.Index);
                    }
                }
            }
        }
    }
}
