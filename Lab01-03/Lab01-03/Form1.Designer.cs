﻿namespace Lab01_03
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.numFirstNum = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.numSecondNum = new System.Windows.Forms.NumericUpDown();
            this.btnSumar = new System.Windows.Forms.Button();
            this.btnRestar = new System.Windows.Forms.Button();
            this.btnMult = new System.Windows.Forms.Button();
            this.btnDiv = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtResultado = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtTemRes = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.radFaren = new System.Windows.Forms.RadioButton();
            this.radCelcius = new System.Windows.Forms.RadioButton();
            this.numTemp = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnRaiz = new System.Windows.Forms.Button();
            this.btnPrimos = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numFirstNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSecondNum)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTemp)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // numFirstNum
            // 
            this.numFirstNum.DecimalPlaces = 2;
            this.numFirstNum.Location = new System.Drawing.Point(110, 31);
            this.numFirstNum.Margin = new System.Windows.Forms.Padding(4);
            this.numFirstNum.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numFirstNum.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numFirstNum.Name = "numFirstNum";
            this.numFirstNum.Size = new System.Drawing.Size(118, 23);
            this.numFirstNum.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 33);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Número 1:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 69);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Número 2:";
            // 
            // numSecondNum
            // 
            this.numSecondNum.DecimalPlaces = 2;
            this.numSecondNum.Location = new System.Drawing.Point(110, 67);
            this.numSecondNum.Margin = new System.Windows.Forms.Padding(4);
            this.numSecondNum.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numSecondNum.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numSecondNum.Name = "numSecondNum";
            this.numSecondNum.Size = new System.Drawing.Size(118, 23);
            this.numSecondNum.TabIndex = 3;
            // 
            // btnSumar
            // 
            this.btnSumar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSumar.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnSumar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSumar.Location = new System.Drawing.Point(54, 113);
            this.btnSumar.Name = "btnSumar";
            this.btnSumar.Size = new System.Drawing.Size(32, 32);
            this.btnSumar.TabIndex = 5;
            this.btnSumar.Text = "+";
            this.btnSumar.UseVisualStyleBackColor = false;
            this.btnSumar.Click += new System.EventHandler(this.BtnSumar_Click);
            // 
            // btnRestar
            // 
            this.btnRestar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnRestar.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnRestar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRestar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRestar.Location = new System.Drawing.Point(92, 113);
            this.btnRestar.Name = "btnRestar";
            this.btnRestar.Size = new System.Drawing.Size(32, 32);
            this.btnRestar.TabIndex = 6;
            this.btnRestar.Text = "-";
            this.btnRestar.UseVisualStyleBackColor = false;
            this.btnRestar.Click += new System.EventHandler(this.BtnRestar_Click);
            // 
            // btnMult
            // 
            this.btnMult.BackColor = System.Drawing.Color.Black;
            this.btnMult.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnMult.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMult.Location = new System.Drawing.Point(130, 113);
            this.btnMult.Name = "btnMult";
            this.btnMult.Size = new System.Drawing.Size(32, 32);
            this.btnMult.TabIndex = 7;
            this.btnMult.Text = "x";
            this.btnMult.UseVisualStyleBackColor = false;
            this.btnMult.Click += new System.EventHandler(this.BtnMult_Click);
            // 
            // btnDiv
            // 
            this.btnDiv.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.btnDiv.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnDiv.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDiv.Location = new System.Drawing.Point(168, 113);
            this.btnDiv.Name = "btnDiv";
            this.btnDiv.Size = new System.Drawing.Size(32, 32);
            this.btnDiv.TabIndex = 8;
            this.btnDiv.Text = "/";
            this.btnDiv.UseVisualStyleBackColor = false;
            this.btnDiv.Click += new System.EventHandler(this.BtnDiv_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 169);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 16);
            this.label3.TabIndex = 9;
            this.label3.Text = "Resultado:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtResultado);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.numFirstNum);
            this.groupBox1.Controls.Add(this.btnDiv);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnMult);
            this.groupBox1.Controls.Add(this.btnRestar);
            this.groupBox1.Controls.Add(this.numSecondNum);
            this.groupBox1.Controls.Add(this.btnSumar);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(258, 213);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Operaciones básicas";
            // 
            // txtResultado
            // 
            this.txtResultado.Location = new System.Drawing.Point(110, 166);
            this.txtResultado.Name = "txtResultado";
            this.txtResultado.ReadOnly = true;
            this.txtResultado.Size = new System.Drawing.Size(118, 23);
            this.txtResultado.TabIndex = 12;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(373, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(94, 98);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtTemRes);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.radFaren);
            this.groupBox2.Controls.Add(this.radCelcius);
            this.groupBox2.Controls.Add(this.numTemp);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(276, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(203, 213);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Conversión Temperatura";
            // 
            // txtTemRes
            // 
            this.txtTemRes.Location = new System.Drawing.Point(109, 163);
            this.txtTemRes.Name = "txtTemRes";
            this.txtTemRes.ReadOnly = true;
            this.txtTemRes.Size = new System.Drawing.Size(87, 23);
            this.txtTemRes.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 166);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 17);
            this.label6.TabIndex = 13;
            this.label6.Text = "Resultado:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 31);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 17);
            this.label5.TabIndex = 16;
            this.label5.Text = "Temperatura:";
            // 
            // radFaren
            // 
            this.radFaren.AutoSize = true;
            this.radFaren.Location = new System.Drawing.Point(40, 123);
            this.radFaren.Name = "radFaren";
            this.radFaren.Size = new System.Drawing.Size(90, 21);
            this.radFaren.TabIndex = 15;
            this.radFaren.TabStop = true;
            this.radFaren.Text = "Farenheit ";
            this.radFaren.UseVisualStyleBackColor = true;
            this.radFaren.CheckedChanged += new System.EventHandler(this.RadFaren_CheckedChanged);
            // 
            // radCelcius
            // 
            this.radCelcius.AutoSize = true;
            this.radCelcius.Location = new System.Drawing.Point(40, 96);
            this.radCelcius.Name = "radCelcius";
            this.radCelcius.Size = new System.Drawing.Size(71, 21);
            this.radCelcius.TabIndex = 14;
            this.radCelcius.TabStop = true;
            this.radCelcius.Text = "Celcius";
            this.radCelcius.UseVisualStyleBackColor = true;
            this.radCelcius.CheckedChanged += new System.EventHandler(this.RadCelcius_CheckedChanged);
            // 
            // numTemp
            // 
            this.numTemp.DecimalPlaces = 2;
            this.numTemp.Location = new System.Drawing.Point(109, 29);
            this.numTemp.Margin = new System.Windows.Forms.Padding(4);
            this.numTemp.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numTemp.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.numTemp.Name = "numTemp";
            this.numTemp.Size = new System.Drawing.Size(87, 23);
            this.numTemp.TabIndex = 13;
            this.numTemp.ValueChanged += new System.EventHandler(this.NumTemp_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 67);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 17);
            this.label4.TabIndex = 13;
            this.label4.Text = "Convertir a:";
            this.label4.Click += new System.EventHandler(this.Label4_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnRaiz);
            this.groupBox3.Controls.Add(this.btnPrimos);
            this.groupBox3.Controls.Add(this.pictureBox1);
            this.groupBox3.Location = new System.Drawing.Point(12, 247);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(467, 105);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Otros";
            // 
            // btnRaiz
            // 
            this.btnRaiz.BackColor = System.Drawing.Color.Black;
            this.btnRaiz.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnRaiz.Location = new System.Drawing.Point(29, 61);
            this.btnRaiz.Name = "btnRaiz";
            this.btnRaiz.Size = new System.Drawing.Size(312, 23);
            this.btnRaiz.TabIndex = 13;
            this.btnRaiz.Text = "Raíz cuadrado de los 10 primeros números";
            this.btnRaiz.UseVisualStyleBackColor = false;
            this.btnRaiz.Click += new System.EventHandler(this.BtnRaiz_Click);
            // 
            // btnPrimos
            // 
            this.btnPrimos.BackColor = System.Drawing.Color.Black;
            this.btnPrimos.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnPrimos.Location = new System.Drawing.Point(29, 32);
            this.btnPrimos.Name = "btnPrimos";
            this.btnPrimos.Size = new System.Drawing.Size(312, 23);
            this.btnPrimos.TabIndex = 12;
            this.btnPrimos.Text = "10 primeros números primos";
            this.btnPrimos.UseVisualStyleBackColor = false;
            this.btnPrimos.Click += new System.EventHandler(this.BtnPrimos_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(491, 370);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Ejercicio1";
            ((System.ComponentModel.ISupportInitialize)(this.numFirstNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSecondNum)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTemp)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numFirstNum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numSecondNum;
        private System.Windows.Forms.Button btnSumar;
        private System.Windows.Forms.Button btnRestar;
        private System.Windows.Forms.Button btnMult;
        private System.Windows.Forms.Button btnDiv;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtResultado;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown numTemp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton radFaren;
        private System.Windows.Forms.RadioButton radCelcius;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTemRes;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnRaiz;
        private System.Windows.Forms.Button btnPrimos;
    }
}

