﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab01_03
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        private void Label4_Click(object sender, EventArgs e)
        {

        }
        //Funciones para cada uno de los botones de la seccion operaciones basicas
        private void BtnSumar_Click(object sender, EventArgs e)
        {
            Double n1 = Convert.ToDouble(numFirstNum.Value);
            Double n2 = Convert.ToDouble(numSecondNum.Value);
            txtResultado.Text = $"{n1 + n2}";
        }

        private void BtnRestar_Click(object sender, EventArgs e)
        {
            Double n1 = Convert.ToDouble(numFirstNum.Value);
            Double n2 = Convert.ToDouble(numSecondNum.Value);
            txtResultado.Text = $"{n1 - n2}";
        }

        private void BtnMult_Click(object sender, EventArgs e)
        {
            Double n1 = Convert.ToDouble(numFirstNum.Value);
            Double n2 = Convert.ToDouble(numSecondNum.Value);
            txtResultado.Text = $"{n1 * n2}";
        }

        private void BtnDiv_Click(object sender, EventArgs e)
        {
            Double n1 = Convert.ToDouble(numFirstNum.Value);
            Double n2 = Convert.ToDouble(numSecondNum.Value);
            txtResultado.Text = $"{n1 / n2}";
        }
        //funcion para mostrar en un alert box los 10 primeros numeros primos
        private void BtnPrimos_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Espere mientras calculamos el resultado...");
            int n = 2;
            List<int> primos = new List<int> { };
            int total = 1;while (total <= 10)
            {
                bool esPrimo = true;
                for (int i = 2; i < n; i++)
                {
                    if (n % i == 0)
                    {
                        esPrimo = false;
                        break;
                    }

                }
                if (esPrimo)
                {
                    primos.Add(n);
                    total++;
                }
                n++;
            }
            Console.WriteLine(primos);
            var message = string.Join(Environment.NewLine, primos.ToArray());
            const string caption = "10 primeros números primos";
            MessageBox.Show(message, caption);
        }
        //funcion para mostrar las 10 primeras raices
        private void BtnRaiz_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Espere mientras calculamos el resultado...");
            List<Double> raices = new List<Double> { };
            for (int i = 1; i <= 10; i++)
            {
                raices.Add(Math.Sqrt(i));
            }
            Console.WriteLine(raices);
            var message = string.Join(Environment.NewLine, raices.ToArray());
            const string caption = "Raíz cuadrada de los 10 primeros números";
            MessageBox.Show(message, caption);
        }
        //funciones para convertir temp de acuerdo al checkbox seleccionado
        private void RadCelcius_CheckedChanged(object sender, EventArgs e)
        {
            if (radCelcius.Checked)
            {
                Double f = Convert.ToDouble(numTemp.Value);
                Double celcius = 5 * (f - 32) / 9;
                txtTemRes.Text = celcius.ToString();
            }
        }

        private void RadFaren_CheckedChanged(object sender, EventArgs e)
        {

            if (radFaren.Checked)
            {
                Double c = Convert.ToDouble(numTemp.Value);
                Double faren = 9 * c / 5 + 32;
                txtTemRes.Text = faren.ToString();
            }
        }

        private void NumTemp_ValueChanged(object sender, EventArgs e)
        {
            if (radCelcius.Checked)
            {
                Double f = Convert.ToDouble(numTemp.Value);
                Double celcius = 5 * (f - 32) / 9;
                txtTemRes.Text = celcius.ToString();
            }
            if (radFaren.Checked)
            {
                Double c = Convert.ToDouble(numTemp.Value);
                Double faren = 9 * c / 5 + 32;
                txtTemRes.Text = faren.ToString();
            }
        }
    }
}
