﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        //Funciones para resolver las 4 operaciones básicas
        static int Suma(int a, int b)
        {
            return a + b;
        }
        static int Resta(int a, int b)
        {
            return a - b;
        }
        static int Mult(int a, int b)
        {
            return a * b;
        }
        static Double Div(int a, int b)
        {
            return a / b;
        }
        //Procedimiento que imprime la raíz cuadrada de los 10 primeros números
        static void Raiz()
        {
            for (int i = 1; i <= 10; i++)
            {
                Console.WriteLine("La raíz cuadrada de {0} es: {1}", i, Math.Sqrt(i));
            }
        }
        //Funcion para hallar los 10 primeros números primos
        static bool EsPrimo(int n)
        {
            for (int d = 2; d < n; d++)
            {
                if (n % d == 0)
                {
                    return false;
                }
            }
            return true;
        }
        static void Primos()
        {
            int n = 2;
            int i = 1;
            while(i <= 10)
            {
                if (EsPrimo(n))
                {
                    Console.WriteLine("{0}°.- {1}", i, n);
                    i++;
                }
                n++;
            }
        }
        //convertir C a F y viceversa
        static Double ConvertTemperature(int t, bool isC)
        {
            if (isC)
            {
                Double f = (9 * t) / 5 + 32;
                return f;
            }
            Double c = 5 * (t - 32) / 9;
            return c;
        }
        static void Main(string[] args)
        {
            Console.Title = "Procedimientos y funciones";
            string opcion;
            do
            {
                Console.Clear();
                Console.WriteLine("[1] Suma de dos números");
                Console.WriteLine("[2] Resta de dos números");
                Console.WriteLine("[3] Multiplicación de dos números");
                Console.WriteLine("[4] División de dos números");
                Console.WriteLine("[5] Imprimir la raíz cuadrada de los 10 primeros números enteros");
                Console.WriteLine("[6] Imprimirlos 10 primeros números primos");
                Console.WriteLine("[7] Convertir Celsius a Fahrenheit");
                Console.WriteLine("[8] Convertir Fahrenheit a Celsius");
                Console.WriteLine("[0] Salir de la aplicación");
                Console.WriteLine("Ingrese una opción y presione ENTER");
                opcion = Console.ReadLine();

                switch (opcion)
                {
                    case "1":
                        Console.WriteLine("Ingrese el primer número");
                        int a = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Ingrese el segundo número");
                        int b = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("La suma de {0} y {1} es {2}", a, b, Suma(a, b));
                        Console.ReadKey();
                        break;
                    case "2":
                        Console.WriteLine("Ingrese el primer número");
                        int c = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Ingrese el segundo número");
                        int d = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("La suma de {0} y {1} es {2}", c, d, Resta(c, d));
                        Console.ReadKey();
                        break;
                    case "3":
                        Console.WriteLine("Ingrese el primer número");
                        int f = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Ingrese el segundo número");
                        int g = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("La suma de {0} y {1} es {2}", f, g, Mult(f, g));
                        Console.ReadKey();
                        break;
                    case "4":
                        Console.WriteLine("Ingrese el primer número");
                        int h = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Ingrese el segundo número");
                        int i = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("La suma de {0} y {1} es {2}", h, i, Div(h, i));
                        Console.ReadKey();
                        break;
                    case "5":
                        Console.WriteLine("Calculando...");
                        Raiz();
                        Console.ReadKey();
                        break;
                    case "6":
                        Console.WriteLine("Calculando...");
                        Primos();
                        Console.ReadKey();
                        break;
                    case "7":
                        Console.WriteLine("Ingrese temperatura en Celsius");
                        int celsius = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("{0} °C son {1} °F", celsius, ConvertTemperature(celsius, true));
                        Console.ReadKey();
                        break;
                    case "8":
                        Console.WriteLine("Ingrese temperatura en Fahrenheit");
                        int fahrenheit = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("{0} °F son {1} °C", fahrenheit, ConvertTemperature(fahrenheit, false));
                        Console.ReadKey();
                        break;
                }
            } while (!opcion.Equals("0"));

        }
    }
}
